using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_EnemyDamage : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //only player collisions 
        if (collision.gameObject.tag == "Player")
        {
            //1 life is removed from the player if a player collides
            collision.gameObject.GetComponent<scp_PlayerLifeManager>().RemoveLife(1);
        }
    }
}

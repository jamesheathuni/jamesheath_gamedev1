using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_EnemyLifeManager : MonoBehaviour
{
    //Enemy life counter
    public int enemyHealth;

    //Reference animation script
    scp_EnemyAnimations animationScript;

    //Check enemy health
    public bool enemyAlive = true;

    //Reference collider
    private CircleCollider2D enemyCollider;

    //Reference game manager
    private scp_Game_Manager gameManager;

    //Counter for explosion duplication avoiding
    int counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        animationScript = GetComponent<scp_EnemyAnimations>();

        //Finds collider
        enemyCollider = GetComponent<CircleCollider2D>();

        //Finds game manager script
        gameManager = FindObjectOfType<scp_Game_Manager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Removes 1 health point after bullet collision
        if(collision.gameObject.tag == "Bullet")
        {
            enemyHealth--;

            EnemyDeath();
        }
    }

    //Destroys the enemy
    void EnemyDeath()
    {
    //Checks if health is greater than 0
    if(enemyHealth <= 0 && enemyAlive == true)
        {
            //Turns off collision once enemy is dead
            enemyCollider.enabled = false; 

            //alive boolean = false
            enemyAlive = false; 

            //Explodes
            animationScript.Explode();

            //Increase destroy counter by 1
            gameManager.enemiesKilledCounter++;

            //Remove 1 from enemys spawned
            gameManager.enemiesSpawned--;

            //Destroys the enemy after a certain time
            //After the animation has finished
            Destroy(this.gameObject, 0.25f);
        }
    }

    public void Explode()
    {
        if(counter == 0)
        {
            //Add 1 to the counter
            //To stop running multiple times
            counter++;

            //Turns off collision once enemy is dead
            enemyCollider.enabled = false; 

            //alive boolean = false
            enemyAlive = false; 

            //Explodes
            animationScript.Explode();

            //Remove 1 from enemys spawned
            gameManager.enemiesSpawned--;

            //Destroys the enemy after a certain time
            //After the animation has finished
            Destroy(this.gameObject, 0.25f);
        }

    }

}

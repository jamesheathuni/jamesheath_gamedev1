using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_EnemyExplodeProximity : MonoBehaviour
{

    //Reference enemy life manager
    public scp_EnemyLifeManager enemyLife;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //when player collides with Collider
        // will activate
        if(collision.gameObject.tag == "Player")
        {
            //Causes the explosion
            enemyLife.Explode();
        }
    }
}

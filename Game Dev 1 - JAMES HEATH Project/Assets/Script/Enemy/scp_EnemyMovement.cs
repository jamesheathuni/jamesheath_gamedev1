using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_EnemyMovement : MonoBehaviour
{
    //Transform of the player
    Transform playerTransform;

    //Enemy speed
    public float enemySpeed;

    //Enemy move boolean
    public bool enemyMove = false;

    //Reference life manager
    scp_EnemyLifeManager lifeManager; 

    //Reference game manager
    scp_Game_Manager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        RandomMovementTime();

        //find script
        lifeManager = GetComponent<scp_EnemyLifeManager>();

        //Finds the player transform programatically
        playerTransform = FindObjectOfType<scp_Player_Movement>().GetComponent<Transform>();

        //Find game manager
        gameManager = FindObjectOfType<scp_Game_Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.gameOver != true)
        {
            if(lifeManager.enemyAlive == true)
            {
                if (enemyMove == true)
                {
                EnemyMovement();
                }
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            //Stop the enemy from moving
            StopEnemyMovement();

            //Make enemy move
            Invoke("AllowEnemyMovement", (1f));
        }
    }

    void EnemyMovement()
    {
        //enemy speed
        float step = enemySpeed * Time.deltaTime;

        //move the enemy
        transform.position = Vector3.MoveTowards(transform.position, playerTransform.position, step);
    }

    void RandomMovementTime()
    {
        Invoke("AllowEnemyMovement", Random.Range(0.2f, 4f));
    }

    void AllowEnemyMovement()
    {
        enemyMove = true;
    }

    void StopEnemyMovement()
    {
        enemyMove = false;
    }
}

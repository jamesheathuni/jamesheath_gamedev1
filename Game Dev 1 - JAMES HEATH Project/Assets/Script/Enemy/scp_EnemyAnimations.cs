using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_EnemyAnimations : MonoBehaviour
{
    //Reference the animator
    private Animator anim;

    //Reference the enemy movement script
    private scp_EnemyMovement enemyMovement;

    //Reference the sprite renderer
    private SpriteRenderer sprite;

    //Reference player transform
    public Transform playerTransform;

    //Reference life script
    scp_EnemyLifeManager enemyLife;




    // Start is called before the first frame update
    void Start()
    {
        //Finds the script
        enemyLife = GetComponent<scp_EnemyLifeManager>();

        //finds animator
        anim = GetComponent<Animator>();

        //finds script
        enemyMovement = GetComponent<scp_EnemyMovement>();

        //finds sprite renderer
        sprite = GetComponent<SpriteRenderer>();

        //Finds the player transform
        playerTransform = FindObjectOfType<scp_Player_Movement>().GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(enemyLife.enemyHealth > 0)
        {
            if (enemyMovement.enemyMove == true)
            {
                anim.SetBool("isMoving", true);
            }
            else
            {
                anim.SetBool("isMoving", false);
            }

            EnemyFlip();
        }


    }

    public void Explode()
    {
        //sets alive bool to false
        anim.SetBool("alive", false);

        //Explodes
        anim.SetTrigger("explode");

        //stops the enemy from moving
        enemyMovement.enemyMove = false;
    }

    void EnemyFlip()
    {
        //flips the enemy
        sprite.flipX = playerTransform.position.x < transform.position.x;
    }

}

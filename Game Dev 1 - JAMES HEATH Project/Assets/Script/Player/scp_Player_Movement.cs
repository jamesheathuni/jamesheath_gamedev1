using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Movement : MonoBehaviour
{
    
    //References the rigidBody2D of the player
    public Rigidbody2D rigidBodyVariable;

    //Movement coordinates 
    private Vector2 movementCoordinates;

    //Player character speed
    public float moveSpeed;

    //Reference to the animator
    Animator anim;

    //Reference the game manager
    scp_Game_Manager gameManager;


    // Start is called before the first frame update
    void Start()
    {
       //Find the animator
       anim = GetComponent<Animator>();

       gameManager = FindObjectOfType<scp_Game_Manager>();

    }

    // Update is called once per frame
    void Update()
    {
        ButtonPress();
        MovementCheck();
    }

    private void FixedUpdate()
    {
        if (gameManager.gameOver != true)
        {
            Movement();
        }
    }


    void Movement()
    {
        //Move the player in the desired direction by checking which button is being pressed, and then moving in that direction at the speed set in the moveSpeed variable
        rigidBodyVariable.AddForce(movementCoordinates * moveSpeed * Time.deltaTime);
    }

    void ButtonPress()
    { 
        //Save the direction as a varaible
        movementCoordinates.x = Input.GetAxisRaw("Horizontal");
        movementCoordinates.y = Input.GetAxisRaw("Vertical");

        Debug.Log(movementCoordinates);
    }

    void MovementCheck()
    {
        if(movementCoordinates != Vector2.zero)
        {
            //Moving
            anim.SetBool("movementBool", true);
        }
        else
        {
            //Not Moving
            anim.SetBool("movementBool", false);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Gun_Movement : MonoBehaviour
{

    //Reference to the rigidbody of the gun
    Rigidbody2D gunRb;

    //Reference the hands
    public Transform handsTransform;

    //Variable which stores the mouse position
    [SerializeField]private Vector2 mousePosition;

    //Stores a reference to the gun angle
    public float angle;

    //Reference the camera
    public Camera cam;

    // Start is called before the first frame update
    void Start()
    {

        //Stores the rigidbody in a variable
        gunRb = GetComponent<Rigidbody2D>();

        
    }

    // Update is called once per frame
    void Update()
    {
        //Stores the mouse position in a variable
        mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);

    }
    
    private void FixedUpdate()
    {

    //Sets the gun to the same position as the hands
    gunRb.position = handsTransform.position;


    GunRotation(); 


    }

    private void GunRotation()
    {

        //Stores the direction from one vector to the other
        Vector2 lookDir = mousePosition - gunRb.position;

        //Uses atan 2 to find the radians and rad2deg to convert them into degrees
        angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg; 
        
        //Set the gun rotation to the same as the angle
        gunRb.rotation = angle;

        //Stores the original scale
        Vector3 originalScale = Vector3.one; 
        
        //Stores the flipped scale
        Vector3 flippedScale = new Vector3(1f, -1f, 1);

       
        if (angle > 90 || angle < -90)
        {
            //Flips the gun
            transform.localScale = flippedScale;
        }
        else
        {
            transform.localScale = originalScale;
        }
    
    }
}

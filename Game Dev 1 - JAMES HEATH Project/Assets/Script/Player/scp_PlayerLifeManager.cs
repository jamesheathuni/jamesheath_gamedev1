using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_PlayerLifeManager : MonoBehaviour
{
    //variable for player life
    public int lives; 

    //heart position
    public Transform heartsPosition;


    //heart prefab reference
    public GameObject heart;

    //List for spawned hearts
    public List<GameObject> spawnedHeartList; 

    //Counter for executions
    int counter = 0;

    //Reference game manager
    scp_Game_Manager gameManager;

    
    // Start is called before the first frame update
    void Start()
    {
        SpawnHearts();

        //Find the game manager
        gameManager = FindObjectOfType<scp_Game_Manager>();

    }

    // Update is called once per frame
    void Update()
    {
    
    }

    void SpawnHearts()
    {
        for (float i = 0; i < lives; i ++)
        {
            //Offset
            Vector3 offset = new Vector3(i/2, 0, 0);
            
            //spawns a heart as a variable
            GameObject spawnedHeart = Instantiate(heart, heartsPosition.position + offset, Quaternion.identity);

            //spawns heart as a child
            //so it follows the character
            spawnedHeart.transform.parent = heartsPosition.transform;

            //Current spawned heart is added to the list
            spawnedHeartList.Add(spawnedHeart);
        }
        

    }

    
    public void RemoveLife(int livesToRemove)
    {
        //If there are more than 0 lives
        if (lives > 0)
        {
            //Decreases life counter by how many lifes are to be removed
            lives -= livesToRemove;
            
            //For loop to remove sprite and list
            for (int i = 0; i < livesToRemove; i++)
            {
                //Destroys sprite
                Destroy(spawnedHeartList[spawnedHeartList.Count - 1]);

                //Remove from list
                spawnedHeartList.Remove(spawnedHeartList[spawnedHeartList.Count-1]);
            }
            
        }

        if (lives <= 0)
        {
            gameManager.GameOver();
        }


    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Explosion" && counter == 0)
        {
            //Increases counter by 1
            counter++;

            //Removes a life
            RemoveLife(1);

            Debug.Log("RemoveLife");
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //Reset the counter
        counter = 0;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Gun_Animation : MonoBehaviour
{

    //Reference the animator
    private Animator animator;

    //Reference shooting
    private scp_Gun_Shooting gunShooting;


    // Start is called before the first frame update
    void Start()
    {
        //Finds the animator
        animator = GetComponent<Animator>();

        //Finds gun shooting
        gunShooting = FindObjectOfType<scp_Gun_Shooting>();
    }

    // Update is called once per frame
    void Update()
    {

        if(gunShooting.bulletCount > 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                //Fire the trigger
                animator.SetTrigger("Recoil");
            }
        }
    
    }
}

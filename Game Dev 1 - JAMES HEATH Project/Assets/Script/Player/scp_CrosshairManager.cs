using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_CrosshairManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Hides the cursor
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Creates cursorPosition variable and then stores the position of the mouse as worldpoints
        Vector2 cursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Uses cursorPosition to move the location of the crosshair
        transform.position = cursorPosition;
    }
}

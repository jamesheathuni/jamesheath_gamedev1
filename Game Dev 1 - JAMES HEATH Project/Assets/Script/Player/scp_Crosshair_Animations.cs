using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Crosshair_Animations : MonoBehaviour
{

    //Reference the animator
    private Animator anim; 

    // Start is called before the first frame update
    void Start()
    {
        //Stores the animator component and stores it into the animator variable
        anim = GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetButtonDown("Fire1"))
        {
            //Fires the trigger
            anim.SetTrigger("Fire");
        }
        
    }
}

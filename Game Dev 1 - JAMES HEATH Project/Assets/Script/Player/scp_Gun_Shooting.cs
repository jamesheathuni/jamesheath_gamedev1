using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Gun_Shooting : MonoBehaviour
{

    //Stores the position of the tip of the barrel
    public Transform tipOfTheBarrel;

    //Reference the projectile
    public GameObject projectile;

    //Variable to control the bullets speed
    public float projectileForce;

    //Reference to the Muzzle Flash
    public GameObject muzzleFlash;

    //Reference the audio manager
    scp_Audio_Manager audio;

    //Reference the characters rigidbody
    public Rigidbody2D playerRb;

    //Counts how many bullets
    public float bulletCount;

    //Reference the game manager
    scp_Game_Manager gameManager;


    // Start is called before the first frame update
    void Start()
    {
        //Stores the audio manager
        audio = FindObjectOfType<scp_Audio_Manager>();

        //Find game manager
        gameManager = FindObjectOfType<scp_Game_Manager>();

    }

    // Update is called once per frame
    void Update()
    { 
        if(gameManager.gameOver != true)
        {
            //Shooting
            if (bulletCount > 0f)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    Shooting();
                    bulletCount = bulletCount - 1;
                }
            }

            if (bulletCount <= 0f)
            {
                if(Input.GetButtonDown("Fire1"))
                {
                    audio.OutOfAmmoSound();
                }
            }

            //Reload
            if (Input.GetKeyDown(KeyCode.R))
            {
                bulletCount = 6;
                audio.ReloadSound();
            }
        }
    }

    void Shooting()
    {

        //Plays gunshot sound
        audio.GunShotSound();

        //Instatiate the bullet and then store it in a varaible
        GameObject instantiatedprojectile = Instantiate(projectile, tipOfTheBarrel.position, tipOfTheBarrel.rotation);

        //Find the rigidbody of the bullet and store it in a variable
        Rigidbody2D projectileRb = instantiatedprojectile.GetComponent<Rigidbody2D>();

        //Add force to the projectile
        projectileRb.AddForce(tipOfTheBarrel.right * projectileForce, ForceMode2D.Impulse);


        //Instantiate Muzzle Flash
        GameObject mFlash = Instantiate(muzzleFlash, tipOfTheBarrel.position, tipOfTheBarrel.rotation);

        //Random roatation
        mFlash.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));

        //Float with random number
        float randomNumber = Random.Range(0.5f, 1.5f);
        
        //Random scale
        mFlash.transform.localScale = new Vector3(randomNumber, randomNumber, 1);

        //Destroys the Muzzle Flash after a fixed time
        Destroy(mFlash, 0.07f);

        //Direction of Shooting
        Vector2 direction = transform.position - instantiatedprojectile.transform.position;

        //Player recoil
        playerRecoil(direction*800);
    }

    void playerRecoil(Vector3 recoilDirection)
    {
        //Applies the recoil to the players rigidbody
        playerRb.AddForce(recoilDirection);
    }
}

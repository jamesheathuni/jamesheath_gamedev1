using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Gun_Projectiles : MonoBehaviour
{
    
    //Reference the explosion animation
    public GameObject collisionExplosion;

    //Variable for the explosion time 
    [SerializeField]float explosionTime;

    //VFX Manager Reference
    scp_VFX_Manager vfx;
    
    //Camera Shake Values
    public float shakeDuration;
    public float shakeStrength;

    private void Start()
    {
        //Stores the VFX script
        vfx = FindObjectOfType<scp_VFX_Manager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        //Shake first
        vfx.CameraShake(shakeDuration, shakeStrength);

        //Instantiates an explosion and then stores it in the explosion Variable
        //The explosion will have a random rotation
        GameObject explosion = Instantiate(collisionExplosion, transform.position, Quaternion.Euler(new Vector3(0,0, Random.Range(0, 360))));

        //Random value
        float randomValue = Random.Range(0.5f, 1.5f);

        //Randomly changes the scale
        explosion.transform.localScale = new Vector3(randomValue, randomValue, 1);
        
        //Destroys the gameobject instantly
        Destroy(gameObject);

        //Destroys the explosion once the animation has finished
        Destroy(explosion, explosionTime);
    }
}

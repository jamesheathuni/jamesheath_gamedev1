using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Player_Flip : MonoBehaviour
{

    //Reference the script
    scp_Gun_Movement gun;

    // Start is called before the first frame update
    void Start()
    {
        //Finds the script and stores it in the variable
        gun = FindObjectOfType<scp_Gun_Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        Flip();
    }

    void Flip()
    {
        //Stores the original scale
        Vector3 originalScale = Vector3.one; 
        
        //Stores the flipped scale
        Vector3 flippedScale = new Vector3(-1, 1, 1);

       
        if (gun.angle > 90 || gun.angle < -90)
        {
            //Flips the player
            transform.localScale = flippedScale;
        }
        else
        {
            transform.localScale = originalScale;
        }
    
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Game_Manager : MonoBehaviour
{

    //enemies killed counter
    public int enemiesKilledCounter = 0;

    //Round counter
    public int roundCounter = 1;

    //Enemies spawned counter
    public int enemiesSpawned;

    //Enemies to be spawned on the first round
    public int enemiesToSpawnCount = 1;

    //Between round counter
    int counter = 0;

    //Reference time manager
    scp_Time_Manager time;

    //Reference enemy spawner
    scp_Enemy_Spawn_Manager enemySpawner;

    //Pause Timer
    public float betweenRoundPause;

    //Bool for managing game over
    public bool gameOver = false;

    //Reference scene loader
    scp_SceneLoaderManager sceneLoader; 




    // Start is called before the first frame update
    void Start()
    {
        //Finds time manager
        time = FindObjectOfType<scp_Time_Manager>();

        //Finds enemey spawner
        enemySpawner = FindObjectOfType<scp_Enemy_Spawn_Manager>();

        //Sets the enemies spawned count to the same as the counter
        enemiesSpawned = enemiesToSpawnCount;

        sceneLoader = FindObjectOfType<scp_SceneLoaderManager>();


        StartRound();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemiesSpawned == 0 && counter == 0)
        {
            //increase counter
            counter++;
            //EndRound
            EndRound();

        }

        if (gameOver == true)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                sceneLoader.SkipTwoScenes();
            }

        }

    }

    void StartRound()
    {
        //Spawn enemies
        enemySpawner.SpawnEnemies(enemiesToSpawnCount);

        //Resets
        enemiesSpawned = enemiesToSpawnCount;

        //Reset counter to 0
        counter = 0;

        //Increase round counter by 1
        roundCounter++;

        //Timer continue
        time.timerRun = true;
    }

    void EndRound()
    {
        //Stop the timer
        time.timerRun = false;

        //New enemies to spawn count
        enemiesToSpawnCount = enemiesToSpawnCount + 2;

        //Start next round after a pause
        Invoke("StartRound", betweenRoundPause);

    }

    public void GameOver()
    {
        gameOver = true;



        
    }
}

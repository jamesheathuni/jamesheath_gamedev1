using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scp_UI_Manager : MonoBehaviour
{
    //Reference enemies destroyed text
    public Text enemiesDestroyedText;

    //Reference game manager
    private scp_Game_Manager gameManager;

    //Reference ammo Text
    public Text ammoText;

    //Reference gun shooting script
    private scp_Gun_Shooting gunShooting;

    //Reference reload text
    public Text reloadText;

    //Reference time manager
    private scp_Time_Manager timeManager;

    //Reference time survived Text
    public Text timeSurvived;

    //Reference end text
    public Text gameOverText;

    //Reference enemiesDestroyed Text
    public Text endEnemiesDestroyedText;

    //Reference time survived Text
    public Text endTimeSurvivedText;

    //float to store survival time
    private string endSurvivalTime;

    //Reference Middle Mouse Text
    public Text middleMouseText;


    // Start is called before the first frame update
    void Start()
    {
        //Find game manager
        gameManager = FindObjectOfType<scp_Game_Manager>();

        //Find gun shooting
        gunShooting = FindObjectOfType<scp_Gun_Shooting>();

        //Find time manager
        timeManager = FindObjectOfType<scp_Time_Manager>();
    }

    // Update is called once per frame
    void Update()
    {   
        if (gameManager.gameOver != true)
        {
            //updates UI for enemies destroyed
            //changes the number to a string
            enemiesDestroyedText.text = gameManager.enemiesKilledCounter.ToString();

            //updates UI for ammo
            ammoText.text = gunShooting.bulletCount.ToString();

            //updates UI to display reload message
            //if the player has 0 bullets left
            if(gunShooting.bulletCount == 0)
            {
                reloadText.text = "PRESS 'R' TO RELOAD";
            }
            else
            {
                reloadText.text = "";
            }

            //updates UI for time
            timeSurvived.text = timeManager.timer.ToString("f1");

            endSurvivalTime = timeManager.timer.ToString("f1");

            gameOverText.text = "";

            endEnemiesDestroyedText.text = "";

            endTimeSurvivedText.text = "";

            middleMouseText.text = "";

        }

        //When game is over
        else
        {
            gameOverText.text = "--- GAME OVER ---";

            endEnemiesDestroyedText.text = "YOU DESTROYED " + gameManager.enemiesKilledCounter.ToString() + " ENEMIES";

            endTimeSurvivedText.text = "YOU SURVIVED " + endSurvivalTime + " SECONDS";

            middleMouseText.text = "PRESS LEFT CLICK TO CONTINUE";

        }

    }
}

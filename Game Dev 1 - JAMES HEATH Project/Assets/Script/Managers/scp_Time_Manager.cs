using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Time_Manager : MonoBehaviour
{
    //Timer
    public float timer;
    
    //Timer Bool - can the timer run?
    public bool timerRun = true;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timerRun == true)
        {
            IncreaseTime();
        }

        
    }

    void IncreaseTime()
    {
        timer += Time.deltaTime;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scp_SceneLoaderManager : MonoBehaviour
{
    //Loads the next scene
    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }

    //Loads the previous scene
    public void LoadPreviousScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-2);
    }

    //Loads the scene that is +2 of the current scene
    //Used for going from the menu to the controls page
    public void SkipTwoScenes()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+2);
    }

    public void  LoadMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-3);
    }


}

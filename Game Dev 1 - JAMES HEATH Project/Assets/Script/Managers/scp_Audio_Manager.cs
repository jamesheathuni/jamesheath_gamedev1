using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Audio_Manager : MonoBehaviour
{
    //Reference the audiosource1
    public AudioSource audioSource1;

    //Reference the aurdiosource2
    public AudioSource audioSource2;

    //Reference the gunshot audio clip
    public AudioClip gunSound;

    //Reference the empty sound
    public AudioClip emptyGunSound;

    //Reference the reload sound
    public AudioClip reloadSound;


    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Plays the gunshot
    public void GunShotSound()
    {

        //Changes the pitch randomly
        audioSource1.pitch = Random.Range(0.8f, 1.5f);        

        audioSource1.PlayOneShot(gunSound);
    }

    public void OutOfAmmoSound()
    {
        //Changes the pitch randomly
        audioSource2.pitch = Random.Range(0.8f, 1.5f);        

        audioSource2.PlayOneShot(emptyGunSound);
    }

    public void ReloadSound()
    {
        audioSource2.pitch = Random.Range(0.8f, 1.5f);        

        audioSource2.PlayOneShot(reloadSound);
    }
}

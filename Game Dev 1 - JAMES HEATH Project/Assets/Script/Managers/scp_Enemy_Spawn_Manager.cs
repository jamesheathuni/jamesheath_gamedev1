using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_Enemy_Spawn_Manager : MonoBehaviour
{
    //Used for the enemy spawnpoints
    public Transform[] spawnPoints;

    //Reference the enemy prefab
    public GameObject enemy;

   


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnEnemies(int enemiesToSpawn)
    {
        for(int i = 0; i < enemiesToSpawn; i++)
        {  
            //Generates a random number
            //between the start of the array and the end
            int randomInt = Random.Range(0, spawnPoints.Length);

            //Instantiates and enemy at the random spawnpoint
            Instantiate(enemy, spawnPoints[randomInt].position, Quaternion.identity);
        }
    }
}

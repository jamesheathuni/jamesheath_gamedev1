using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class scp_VFX_Manager : MonoBehaviour
{
    //Reference the camera
    [SerializeField]Camera cam;

    //Shake strength
    public float camShakeStrength;

    //Shake duration
    public float camShakeDuration;
    
    //Stores the original camera position
    Vector3 originalCameraPosition;

    //Reference gun shooting
    scp_Gun_Shooting gunShooting;

    // Start is called before the first frame update
    void Start()
    {
        //Stores the camera in the variable
        cam = Camera.main;
        
        //Returns the camera to the original position
        originalCameraPosition = cam.transform.position;    
        
        //find gun shooting script
        gunShooting = FindObjectOfType<scp_Gun_Shooting>();
    }

    // Update is called once per frame
    void Update()
    {
        //Only shakes when there is more than 0 bullets
        if(gunShooting.bulletCount > 0)
            if (Input.GetButtonDown("Fire1"))
            {
                CameraShake(camShakeDuration, camShakeStrength);

            }
    }

    public void CameraShake(float shakeDuration, float shakeStrength)
    {
        //Shakes the camera
        cam.transform.DOShakePosition(shakeDuration, shakeStrength);


        Invoke("CameraPositionReset", 0.35f);
    }

    void CameraPositionReset()
    {
    cam.transform.DOMove(originalCameraPosition, 0.3f);
    }
}
